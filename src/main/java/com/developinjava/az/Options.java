/*
 Copyright 2014 David Salter

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package com.developinjava.az;

/**
 *
 * @author <david@developinjava.com>
 */
public class Options {
    
    private static Options instance = null;
    private Boolean displayDirectories = true;
    
    protected Options() {
    }

    public Boolean getDisplayDirectories() {
        return displayDirectories;
    }

    public void setDisplayDirectories(Boolean displayDirectories) {
        this.displayDirectories = displayDirectories;
    }
    
    public static Options getInstance() {
        if (instance == null) {
            instance = new Options();
        }
        
        return instance;
    }
    
}
