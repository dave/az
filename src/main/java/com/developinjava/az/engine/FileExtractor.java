/*
 Copyright 2014 David Salter

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package com.developinjava.az.engine;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.compress.archivers.ArchiveException;
import org.apache.commons.compress.archivers.ArchiveInputStream;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.utils.IOUtils;

/**
 *
 * @author <david@developinjava.com>
 */
public class FileExtractor {
    
    private String archiveFile;
    
    public FileExtractor(String archiveFile) {
        this.archiveFile = archiveFile;
    }
    
    public String getArchiveFile() {
        return archiveFile;
    }
    
    public String extract(String file) {
        String tempFileName = null;
        File outputFile = null;
        ArchiveInputStream ais = null;
        InputStream is = null;
        try {
            is = new FileInputStream(getArchiveFile());
            ais = new ArchiveStreamFactory().createArchiveInputStream(ArchiveStreamFactory.ZIP, is);
            ZipArchiveEntry entry = (ZipArchiveEntry)ais.getNextEntry();
            while (entry != null) {
                if (entry.getName().equalsIgnoreCase(file)) {
                    outputFile = File.createTempFile("aztmp", ".tmp");
                    tempFileName = outputFile.getAbsolutePath();
                    OutputStream os = new FileOutputStream(outputFile);
                    IOUtils.copy(ais, os);
                    os.close();
                    break;
                }
                entry = (ZipArchiveEntry)ais.getNextEntry();
            }
            ais.close();
            is.close();
        } catch (ArchiveException | IOException e) {
            
        } finally {
            if (ais != null) {
                try {
                    ais.close();
                } catch (IOException ex) {
                    Logger.getLogger(FileExtractor.class.getName()).log(Level.INFO, null, ex);
                }
            }
            if (is != null) {
                try {
                    is.close();
                } catch (IOException ex) {
                    Logger.getLogger(FileExtractor.class.getName()).log(Level.INFO, null, ex);
                }
            }
        }
        
        return tempFileName;
    }
}
