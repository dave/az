/*
 Copyright 2014 David Salter

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package com.developinjava.az.gui.model;

import com.developinjava.az.Options;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.swing.table.AbstractTableModel;
import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.ArchiveInputStream;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;

/**
 *
 * @author <david@developinjava.com>
 */
public class ArchiveEntryModel extends AbstractTableModel {

    private String[] columnNames = {"Name", "Size", "Compressed", "Ratio", "Date Modified"};
    private String archiveFile;
    private ArrayList<ArchiveEntry> entries = new ArrayList<ArchiveEntry>();

    /**
     * Public constructor. The only way to construct a JarEntryModel specifying
     * a Jar file to analyse.
     *
     * @param file The Jar file to analyse.
     */
    public ArchiveEntryModel(String file) {
        this.archiveFile = file;
        initialise();
    }

    /**
     * Get the number of rows in the table (i.e. number of files in the Jar
     * file).
     *
     * @return Rows.
     */
    @Override
    public int getRowCount() {
        return entries.size();
    }

    /**
     * Get the number of columns in the table. This aways returns 2. The first
     * column represents the class name whereas the second column represents the
     * package name.
     *
     * @return Columns.
     */
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    /**
     * Gets a specific entry (class name or package name) in the table model.
     *
     * @param rowIndex Row number.
     * @param columnIndex Column number.
     * @return The value at the specified (row,column).
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        ArchiveEntry entry = entries.get(rowIndex);

        if (columnIndex == 0) {
            return entry.getName();
        } else if (columnIndex == 1) {
            return entry.getSize();
        } else if (columnIndex == 2) {
            if (entry instanceof ZipArchiveEntry) {
                ZipArchiveEntry zip = (ZipArchiveEntry) (entry);
                return zip.getCompressedSize();
            } else {
                return "";
            }
        } else if (columnIndex == 3) {
            if (entry.getSize() == 0) {
                return "-";
            } else {
                ZipArchiveEntry zip = (ZipArchiveEntry) (entry);
                return ((100 * zip.getCompressedSize() / zip.getSize())) + "%";
            }
        } else {
            DateFormat formatter = DateFormat.getDateTimeInstance(
                    DateFormat.SHORT,
                    DateFormat.SHORT,
                    Locale.getDefault());
            return formatter.format(entry.getLastModifiedDate());
        }
    }

    /**
     * Gets the name of the specified column.
     *
     * @param col Column number
     * @return Name of the column.
     */
    @Override
    public String getColumnName(int col) {
        return columnNames[col];
    }

    /**
     * Gets the Jar file and extracts all classes names and package names for
     * all the files in the Jar file. These entries are then stored in an
     * ArrayList of JarEntry objects.
     */
    private void initialise() {
        Options options = Options.getInstance();
        
        try {
            final File input = new File(archiveFile);

            List<String> results = new ArrayList<String>();

            final InputStream is = new FileInputStream(input);
            ArchiveInputStream in = null;
            try {
                in = new ArchiveStreamFactory().createArchiveInputStream(ArchiveStreamFactory.ZIP, is);

                ArchiveEntry entry = null;
                while ((entry = (ArchiveEntry) in.getNextEntry()) != null) {
                    if (entry.isDirectory()) {
                        if (options.getDisplayDirectories()==true) {
                            entries.add(entry);
                        }
                    } else {
                        entries.add(entry);
                    }

                    ArchiveInputStream nestedIn = new ArchiveStreamFactory().createArchiveInputStream(ArchiveStreamFactory.ZIP, in);
                    ArchiveEntry nestedEntry = null;
                    while ((nestedEntry = (ArchiveEntry) nestedIn.getNextEntry()) != null) {
                        results.add(nestedEntry.getName());
                    }
                    // nested stream must not be closed here
                }
            } finally {
                if (in != null) {
                    in.close();
                }
            }
            is.close();
        } catch (Exception e) {

        }
    }
}
